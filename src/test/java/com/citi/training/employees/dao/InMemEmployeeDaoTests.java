package com.citi.training.employees.dao;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.employees.exceptions.EmployeeNotFoundException;
import com.citi.training.employees.model.Employee;

public class InMemEmployeeDaoTests {

    private static Logger LOG = LoggerFactory.getLogger(InMemEmployeeDaoTests.class);

    private int testId = 879;
    private String testName = "Bob Smith";
    private double testSalary = 123.99;

    @Test
    @Transactional
    public void test_saveAndGetEmployee() {
        Employee testEmployee = new Employee(-1, testName, testSalary);
        InMemEmployeeDao testRepo = new InMemEmployeeDao();

        testEmployee = testRepo.create(testEmployee);
 
        assertTrue(testRepo.findById(testEmployee.getId()).equals(testEmployee));

    }

    @Test
    @Transactional
    public void test_saveAndGetAllEmployees() {
        Employee[] testEmployeeArray = new Employee[100];
        InMemEmployeeDao testRepo = new InMemEmployeeDao();

        for(int i=0; i<testEmployeeArray.length; ++i) {
            testEmployeeArray[i] = new Employee(-1, testName, testSalary);

            testRepo.create(testEmployeeArray[i]);
        }

        List<Employee> returnedEmployees = testRepo.findAll();
        LOG.info("Received [" + returnedEmployees.size() +
                 "] Employees from repository");

        for(Employee thisEmployee: testEmployeeArray) {
            assertTrue(returnedEmployees.contains(thisEmployee));
        }
        LOG.info("Matched [" + testEmployeeArray.length + "] Employees");
    }

    @Test (expected = EmployeeNotFoundException.class)
    @Transactional
    public void test_deleteEmployee() {
    	Employee[] testEmployeeArray = new Employee[100];
        InMemEmployeeDao testRepo = new InMemEmployeeDao();

        for(int i=0; i<testEmployeeArray.length; ++i) {
            testEmployeeArray[i] = new Employee(testId + i, testName, testSalary);

            testRepo.create(testEmployeeArray[i]);
        }
        Employee removedEmployee = testEmployeeArray[5];
    	testRepo.deleteById(removedEmployee.getId());
    	LOG.info("Removed item from Employee array");
    	testRepo.findById(removedEmployee.getId());
    }
}


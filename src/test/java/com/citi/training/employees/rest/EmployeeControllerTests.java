package com.citi.training.employees.rest;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.employees.dao.EmployeeDao;
import com.citi.training.employees.model.Employee;
import com.fasterxml.jackson.databind.ObjectMapper;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTests {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeControllerTests.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeDao mockEmployeeDao;

    @Test
    @Transactional
    public void findAllEmployees_returnsList() throws Exception {
        when(mockEmployeeDao.findAll()).thenReturn(new ArrayList<Employee>());

        MvcResult result = this.mockMvc.perform(get("/employees")).andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber()).andReturn();

        logger.info("Result from EmployeeDao.findAll: " +
                    result.getResponse().getContentAsString());
    }

    @Test
    @Transactional
    void createEmployee_returnsCreated() throws Exception {
        Employee testEmployee = new Employee(5, "Bob", 99999.99);

        this.mockMvc
                .perform(post("/employees").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(testEmployee)))
                .andExpect(status().isCreated()).andReturn();
        logger.info("Result from Create Employee");
    }

    @Test
    @Transactional
    public void deleteEmployee_returnsOK() throws Exception {
        MvcResult result = this.mockMvc.perform(delete("/employees/5"))
                                       .andExpect(status().isNoContent())
                                       .andReturn();

        logger.info("Result from EmployeeDao.delete: " +
                    result.getResponse().getContentAsString());
    }

    @Test
    @Transactional
    public void getEmployeeById_returnsOK() throws Exception {
        Employee testEmployee = new Employee(1, "John", 23.3);

        when(mockEmployeeDao.findById(testEmployee.getId())).thenReturn(testEmployee);

        MvcResult result = this.mockMvc.perform(get("/employees/1"))
                                       .andExpect(status().isOk()).andReturn();

        logger.info("Result from EmployeeDao.getEmployee: " +
                    result.getResponse().getContentAsString());
    }

}
